package education.kernet.konectia.caller;


import java.util.ArrayList;

import education.kernet.konectia.model.Album;

public interface IAlbumsDownloadDone {
    void processResult(ArrayList<Album> albumArrayList);
}
