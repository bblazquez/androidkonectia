package education.kernet.konectia.json;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public abstract class JSONSerializer {

    private static final String DATA = "data";

    private JSONSerializer() {
    }

    public static String serialize(Object entity, Class<?> type) {
        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        String objJson = new Gson().toJson(entity, type);

        jsonObject.addProperty(DATA, objJson);
        return jsonObject.toString();
    }
}
