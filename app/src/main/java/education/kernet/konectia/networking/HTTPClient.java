package education.kernet.konectia.networking;

import android.util.Log;

import java.io.IOException;

import education.kernet.konectia.json.JSONDeserializer;
import education.kernet.konectia.model.User;
import education.kernet.konectia.utils.JSONUtils;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HTTPClient extends OkHttpClient {

    private OkHttpClient client;
    public static final MediaType CONTENT_TYPE_JSON = MediaType.parse("application/json");

    public HTTPClient() {
        client = new OkHttpClient();
    }

    public Response get(String url) throws Exception {

        Request request;
            request = new Request.Builder()
                    .url(Configuration.WS_DEV_URL_CONFIG_KEY + url)
                    .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        return response;
    }

    public User loginPost(String endpoint, String jsonContent) throws Exception {

        RequestBody body = RequestBody.create(CONTENT_TYPE_JSON, jsonContent);
        Request request = new Request.Builder()
                .url(Configuration.WS_DEV_URL_CONFIG_KEY + endpoint)
                .post(body)
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        String responseBody = response.body().string();

        Log.d("PDV", "syncPost - onResponse HTTP Code: " + response.code());
        Log.d("PDV", "syncPost - onResponse: " + responseBody);

        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        if (!JSONUtils.isJSONValid(responseBody)) throw new IOException("JSON Not valid!!");

        User returnedUser = JSONDeserializer.deserialize(responseBody, User.class);
        return returnedUser;
    }
}