package education.kernet.konectia.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import education.kernet.konectia.KonectiaApp;
import education.kernet.konectia.R;
import education.kernet.konectia.caller.IOnAlbumToFavorites;
import education.kernet.konectia.model.Album;
import education.kernet.konectia.utils.ApplicationHolder;


public class AlbumDetailsFragment extends Fragment {

    public static final String ALBUM_TYPE_REQUEST = "ALBUM_REQUEST";

    private IOnAlbumToFavorites caller;
    private KonectiaApp app;
    private Album album;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IOnAlbumToFavorites) {
            caller = (IOnAlbumToFavorites) context;
        } else {
            throw new ClassCastException (context.toString()
            + " must implement IOnAlbumToFavorites");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = ApplicationHolder.getApplication();
        album = app.getWorkingAlbum();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details_album, container, false);

        setUpViews(view);
        return view;
    }

    private void setUpViews(View view) {
        TextView titleTextView = (TextView) view.findViewById(R.id.album_title);
        titleTextView.setText(album.getTitle());
    }
}
