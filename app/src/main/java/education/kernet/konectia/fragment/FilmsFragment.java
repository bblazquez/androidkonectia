package education.kernet.konectia.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import education.kernet.konectia.KonectiaApp;
import education.kernet.konectia.R;
import education.kernet.konectia.activity.DetailsActivity;
import education.kernet.konectia.activity.TabsActivity;
import education.kernet.konectia.adapter.FilmListAdapter;
import education.kernet.konectia.caller.IFilmsDownloadDone;
import education.kernet.konectia.caller.IOnFilmClickListener;
import education.kernet.konectia.model.Film;
import education.kernet.konectia.task.FilmsDownloadTask;
import education.kernet.konectia.utils.ApplicationHolder;

import static education.kernet.konectia.fragment.FilmDetailsFragment.FILM_TYPE_REQUEST;


public class FilmsFragment extends Fragment implements IFilmsDownloadDone {

    private RecyclerView recyclerView;
    private FilmListAdapter filmListAdapter;
    private TextView emptyTextView;
    private KonectiaApp app;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = ApplicationHolder.getApplication();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movies, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.film_list);
        emptyTextView = (TextView) view.findViewById(R.id.empty_view);

        loadData();

        return view;
    }

    private void loadData() {
        FilmsDownloadTask filmsDownloadTask = new FilmsDownloadTask((TabsActivity) getActivity(), FilmsFragment.this);
        filmsDownloadTask.execute();
    }

    @Override
    public void processResult(ArrayList<Film> filmArrayList) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        filmListAdapter = new FilmListAdapter(filmArrayList, getActivity(), new IOnFilmClickListener() {
            @Override
            public void onItemClick(Film item) {
                app.setWorkingFilm(item);
                callDetailsActivity();
            }
        });

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(filmListAdapter);

        if (filmArrayList.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            emptyTextView.setText(getString(R.string.no_data_available));
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyTextView.setVisibility(View.GONE);
        }
    }

    private void callDetailsActivity() {
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.putExtra(DetailsActivity.EXTRA_REQUEST_TYPE, FILM_TYPE_REQUEST);
        startActivity(intent);
    }
}
