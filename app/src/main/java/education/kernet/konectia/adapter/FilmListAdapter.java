package education.kernet.konectia.adapter;


import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import education.kernet.konectia.R;
import education.kernet.konectia.caller.IOnFilmClickListener;
import education.kernet.konectia.model.Film;

public class FilmListAdapter extends RecyclerView.Adapter<FilmListAdapter.MyViewHolder> {

    private final ArrayList<Film> films;
    private final Context context;
    private final IOnFilmClickListener listener;

    public FilmListAdapter(ArrayList<Film> films, Context context, IOnFilmClickListener listener)  {
        this.films = films;
        this.context = context;
        this.listener = listener;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView director;
        public TextView year;
        public TextView genre;
        public ImageView poster;
        public View listView;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.item_title);
            director = (TextView) itemView.findViewById(R.id.item_director);
            year = (TextView) itemView.findViewById(R.id.item_year);
            genre = (TextView) itemView.findViewById(R.id.item_genre);
            poster = (ImageView)  itemView.findViewById(R.id.item_poster);
            listView = itemView;
        }

        public void bind (final Film item, final IOnFilmClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
            Glide.with(itemView.getContext()).load(item.getPoster()).into(poster);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.film_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Film film = films.get(position);
        holder.bind(film, listener);

        holder.title.setText(film.getTitle());
        holder.director.setText(film.getDirector());
        holder.year.setText(film.getYear());
        holder.genre.setText(film.getGenre());
    }

    @Override
    public int getItemCount() {
        return films.size();
    }
}
