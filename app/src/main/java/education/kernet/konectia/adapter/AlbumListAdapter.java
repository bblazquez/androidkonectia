package education.kernet.konectia.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import education.kernet.konectia.R;
import education.kernet.konectia.caller.IOnAlbumClickListener;
import education.kernet.konectia.model.Album;
import education.kernet.konectia.model.Film;

public class AlbumListAdapter extends RecyclerView.Adapter<AlbumListAdapter.MyViewHolder> {

    private final ArrayList<Album> albums;
    private final Context context;
    private final IOnAlbumClickListener listener;

    public AlbumListAdapter(ArrayList<Album> albums, Context context, IOnAlbumClickListener listener)  {
        this.albums = albums;
        this.context = context;
        this.listener = listener;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView released;
        public TextView label;
        public ImageView cover;
        public View listView;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.item_title);
            released = (TextView) itemView.findViewById(R.id.item_released);
            label = (TextView) itemView.findViewById(R.id.item_label);
            cover = (ImageView) itemView.findViewById(R.id.item_cover);
            listView = itemView;
        }

        public void bind (final Album item, final IOnAlbumClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
            Glide.with(itemView.getContext()).load(item.getCoverURL()).into(cover);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Album album = albums.get(position);
        holder.bind(album, listener);

        holder.title.setText(album.getTitle());
        holder.released.setText(album.getReleased());
        holder.label.setText(album.getLabel());
    }

    @Override
    public int getItemCount() {
        return albums.size();
    }
}
