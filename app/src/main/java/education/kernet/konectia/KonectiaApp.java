package education.kernet.konectia;


import android.app.Application;

import education.kernet.konectia.model.Album;
import education.kernet.konectia.model.Film;
import education.kernet.konectia.utils.ApplicationHolder;


public class KonectiaApp extends Application {

    private Film workingFilm;
    private Album workingAlbum;

    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationHolder.setApplication(this);
    }

    public Film getWorkingFilm() {
        return workingFilm;
    }

    public void setWorkingFilm(Film workingFilm) {
        this.workingFilm = workingFilm;
    }

    public Album getWorkingAlbum() {
        return workingAlbum;
    }

    public void setWorkingAlbum(Album workingAlbum) {
        this.workingAlbum = workingAlbum;
    }
}
