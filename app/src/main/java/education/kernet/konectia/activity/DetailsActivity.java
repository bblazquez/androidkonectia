package education.kernet.konectia.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import education.kernet.konectia.R;
import education.kernet.konectia.caller.IOnAlbumToFavorites;
import education.kernet.konectia.caller.IOnFilmToFavorites;
import education.kernet.konectia.fragment.AlbumDetailsFragment;
import education.kernet.konectia.fragment.FilmDetailsFragment;
import education.kernet.konectia.model.Album;
import education.kernet.konectia.model.Film;

import static education.kernet.konectia.fragment.AlbumDetailsFragment.ALBUM_TYPE_REQUEST;
import static education.kernet.konectia.fragment.FilmDetailsFragment.FILM_TYPE_REQUEST;


public class DetailsActivity extends AppCompatActivity implements IOnFilmToFavorites, IOnAlbumToFavorites {
    public static String EXTRA_REQUEST_TYPE = "EXTRA_REQUEST_ID";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        String typeRequest = getIntent().getStringExtra(EXTRA_REQUEST_TYPE);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (FILM_TYPE_REQUEST.equalsIgnoreCase(typeRequest)) {
            ft.replace(R.id.placeHolder, new FilmDetailsFragment());
        } else if (ALBUM_TYPE_REQUEST.equalsIgnoreCase(typeRequest)){
            ft.replace(R.id.placeHolder, new AlbumDetailsFragment());
        }
        ft.commit();
    }

    @Override
    public void onFilmToFavorites(Film film) {
        // TODO add to DB
    }

    @Override
    public void onAlbumToFavorites(Album album) {
        // TODO add to DB
    }
}
