package education.kernet.konectia.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import education.kernet.konectia.R;
import education.kernet.konectia.activity.TabsActivity;
import education.kernet.konectia.caller.IFilmsDownloadDone;
import education.kernet.konectia.fragment.FilmsFragment;
import education.kernet.konectia.json.JSONDeserializer;
import education.kernet.konectia.model.Film;
import education.kernet.konectia.networking.HTTPClient;
import education.kernet.konectia.networking.Keys;
import education.kernet.konectia.networking.WSEndpoints;
import education.kernet.konectia.utils.JSONUtils;
import okhttp3.Response;


public class FilmsDownloadTask extends AsyncTask<Void, String, String> {
    private Activity activity;
    private IFilmsDownloadDone caller;
    private ProgressDialog dialog;
    private boolean forceCancel;
    private ArrayList<Film> filmsResult;
    private WeakReference<TabsActivity> context;

    private boolean failure;

    public FilmsDownloadTask(TabsActivity activity, FilmsFragment caller) {
        this.activity = activity;
        this.caller = caller;
        this.context = new WeakReference<>(activity);

        this.filmsResult = new ArrayList<>();
        this.failure = false;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        this.dialog = ProgressDialog.show(activity
                , activity.getString(R.string.film_sync)
                , activity.getString(R.string.sync_msg)
                , true
                , true
                , new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        if (FilmsDownloadTask.this.isCancelled()) {
                            FilmsDownloadTask.this.forceCancel = true;
                        } else {
                            FilmsDownloadTask.this.cancel(true);
                        }
                    }
                });
        this.dialog.setCanceledOnTouchOutside(false);
    }

    @Override
    protected String doInBackground(Void... params) {
        try {
            fetchData();
        } catch (Exception e) {
            e.printStackTrace();
            failure = true;
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        if (activity != null && !activity.isFinishing()) {
            this.dialog.cancel();

            if (!failure) {
                caller.processResult(filmsResult);
            } else {
                warningDialog();
            }
        }
    }

    private void fetchData() {
        try {
            Response response;
            HTTPClient client = new HTTPClient();
            response = client.get(WSEndpoints.FILMS);

            if (response.isSuccessful() && !response.toString().isEmpty()) {
                parseResponse(response, Keys.FILMS);
            }
        } catch (Exception e) {
            e.printStackTrace();
            failure = true;
        }
    }

    private void parseResponse(Response response, String key) {
        try {
            String responseString;
            responseString = response.body().string();
            response.close();

            filmsResult = JSONDeserializer.deserializeList(responseString, Film.class, key);

        } catch (Exception e) {
            e.printStackTrace();
            failure = true;
        }
    }

    private void warningDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.context.get());

        builder.setTitle(activity.getString(R.string.download_not_complete));
        builder.setMessage(activity.getString(R.string.sync_failure));

        builder.setPositiveButton(
                activity.getString(R.string.accept),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

}
